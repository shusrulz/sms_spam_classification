import ktrain
predictor = ktrain.load_predictor('/home/shushant/Desktop/sms_spam_classification/bert_model')

#sample dataset to test on

data = ['Where are you? Call me as soon as possible',
        'Congratulations you won a lottery of 60000$. Please confirm your identity to claim the money and other exciting gifts.',
        'Free entry in 2 a wkly comp to win FA Cup final tkts 21st May 2005. Text FA to 87121 to receive entry question(std txt rate)T&Cs apply 08452810075over18s']

results = predictor.predict(data)
print(results)